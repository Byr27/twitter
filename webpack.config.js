const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = env => ({
  mode: env,
  devtool: 'none',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  plugins: [
    new VueLoaderPlugin(),
    new StyleLintPlugin({
      files: 'src/**/*.(css|scss|vue)',
    }),
  ],
  resolve: {
    extensions: ['.js', '.vue', '.scss'],
    alias: {
      '@global': path.resolve(__dirname, 'src/global'),
      '@components': path.resolve(__dirname, 'src/global/components'),
      '@features': path.resolve(__dirname, 'src/global/features'),
      '@images': path.resolve(__dirname, 'src/images'),
      '@pages': path.resolve(__dirname, 'src/pages'),
      '@mixins': path.resolve(__dirname, 'src/global/js/mixins'),
      '@model': path.resolve(__dirname, 'src/global/js/model'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(ttf|eot|woff|woff2|svg)$/,
        use: [
          // {
          //   loader: 'file-loader',
          //   options: {
          //     name: '[name].[ext]',
          //     outputPath: 'fonts/',
          //   },
          // },
          {
            loader: 'url-loader',
            options: {
              limit: 0,
              name: 'fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(jpg|png|gif|jpeg|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          // {
          //   loader: 'file-loader',
          //   options: {
          //     name: '[name].[ext]',
          //     outputPath: 'images/',
          //   },
          // },
          {
            loader: 'url-loader',
            options: {
              limit: 0,
              mimetype: 'image/png',
            },
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: 'vue-loader',
      },
      {
        test: /\.(css|scss)$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(js|vue)$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },

    ],
  },
});
