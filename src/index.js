import Vue from 'vue';
import VueResource from 'vue-resource';

import App from './App';
import store from './store';
import router from './router';

import 'normalize.css';
import 'flexboxgrid';
import 'material-design-icons/iconfont/material-icons.css';

Vue.use(VueResource);

const app = new Vue({
  store,
  router,
  render: h => h(App),
});

app.$mount('#app');
