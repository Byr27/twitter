export default {
  loggedInUser: {
    first_name: 'Pavel',
    last_name: 'byr27',
    avatar: 'https://cs8.pikabu.ru/post_img/big/2017/06/03/5/1496470979161945395.jpg',
  },
  foundUsers: [],
  users: [],
  posts: [],
  topics: [
    {
      id: 1,
      name: 'Украина',
      location: 'Актуально в Украине',
    },
    {
      id: 2,
      name: 'Мир',
      location: 'Актуально в мире',
    },
    {
      id: 3,
      name: 'Наука',
      location: 'Актуально в мире',
    },
    {
      id: 4,
      name: 'Техника',
      location: 'Актуально в мире',
    },
    {
      id: 5,
      name: 'США',
      location: 'Актуально в мире',
    },
  ],
};
