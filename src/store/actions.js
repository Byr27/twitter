import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

export default {
  getUsers({ commit, state }) {
    if (state.users.length !== 0) return Promise.resolve();

    return Vue.http.get('https://reqres.in/api/users?page=3')
      .then(({ body }) => {
        commit('updateState', { users: body.data });
      });
  },

  searchUsers({ commit }) {
    return Vue.http.get('https://reqres.in/api/users')
      .then(({ body }) => {
        commit('updateState', { foundUsers: body.data });
      });
  },

  getFeeds({ commit }) {
    commit('updateState', {
      posts: [
        {
          id: 1,
          author: {
            id: 1,
            name: 'Лепра))',
            login: 'leprasorium',
            avatar: 'http://kilobank.ru/wp-content/uploads/2017/08/telegram-channel-leprame.jpg',
          },
          text: 'Винни Пух',
          media: 'https://regnum.ru/uploads/pictures/news/2018/08/13/regnum_picture_153414067793039_big.jpg',
          type: 'img',
          commentsAmount: 16,
          reposts: 14,
          likes: 11,
        },
        {
          id: 2,
          author: {
            id: 2,
            name: 'Испаниия',
            login: 'Spain',
            avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/250px-Flag_of_Spain.svg.png',
          },
          text: 'Карта испании',
          media: 'https://tripmydream.cc/travelhub/travel/blocks/11/016/block_11016.jpg',
          type: 'img',
          commentsAmount: 23,
          reposts: 12,
          likes: 15,
        },
      ],
    });

    return Promise.resolve();
  },
};
