export default model => (data = {}) => Object.entries(model)
  .map(([key, value]) => [
    key,
    data[key] !== undefined ? data[key] : value,
  ])
  .reduce((object, [key, value]) => ({
    ...object,
    [key]: value,
  }), {});
