import Vue from 'vue';
import VueRouter from 'vue-router';

import TheHome from '@pages/TheHome';
import TheExplore from '@pages/TheExplore';
import TheNotifications from '@pages/TheNotifications';
import TheMessages from '@pages/TheMessages';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      component: TheHome,
    },
    {
      path: '/explore',
      component: TheExplore,
    },
    {
      path: '/notifications',
      component: TheNotifications,
    },
    {
      path: '/messages',
      component: TheMessages,
    },
  ],
});
